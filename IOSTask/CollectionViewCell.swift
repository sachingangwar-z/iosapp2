//
//  CollectionViewCell.swift
//  IOSTask
//
//  Created by Zolo on 31/01/23.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ImageView: UIImageView!
    
    @IBOutlet weak var TitleView: UILabel!
    
    @IBOutlet weak var descView: UILabel!
    
    func addData(with data: Data){
        ImageView.image=data.image
        TitleView.text=data.title
        descView.text=data.desc
    }
}
