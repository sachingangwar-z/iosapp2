//
//  Data.swift
//  IOSTask
//
//  Created by Zolo on 31/01/23.
//

import Foundation
import UIKit

struct Data{
    let title: String
    let desc: String
    let image: UIImage
}

let data:[Data]=[
    Data(title: "Naruto Uzumaki", desc: "Naruto", image: #imageLiteral(resourceName: "naruto.jpeg")),
    Data(title: "Sasuke Uchiha", desc: "Naruto", image: #imageLiteral(resourceName: "sasuke.jpeg")),
    Data(title: "Kakashi Hatake", desc: "Naruto", image: #imageLiteral(resourceName: "kakashi.jpeg")),
    Data(title: "Captain Levi", desc: "Attack on Titan", image: #imageLiteral(resourceName: "levi.jpeg")),
    Data(title: "Eren Yeager", desc: "Attack on Titan", image: #imageLiteral(resourceName: "eren.jpeg")),
    Data(title: "Mikasha", desc: "Attack on Titan", image: #imageLiteral(resourceName: "mikasa.jpeg")),
    Data(title: "Edward Elric", desc: "FullMetal Alchemist", image: #imageLiteral(resourceName: "edward.jpeg")),
    Data(title: "Roy Mustang", desc: "FullMetal Alchemist", image: #imageLiteral(resourceName: "roy.jpeg")),
    
    Data(title: "Light Yagami", desc: "DeathNote", image: #imageLiteral(resourceName: "light.jpeg")),
    Data(title: "L", desc: "DeathNote", image: #imageLiteral(resourceName: "l.jpeg")),
]


